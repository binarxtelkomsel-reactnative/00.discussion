import React, { useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Linking
} from 'react-native';

const App: () => React$Node = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <StatusBar barStyle="dark-content" />
      <TouchableOpacity
        onPress={() => {
          
          Linking.openURL('introduction')
        }}
      ><Text>Open Photo</Text></TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          Linking.openURL('http://news.apple.com')
        }}
      ><Text>Open News</Text></TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          Linking.openSettings()
        }}
      ><Text>Open Settings</Text></TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          Linking.openURL('http://health.apple.com')
        }}
      ><Text>Open Health</Text></TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          // Linking.openURL('maps://')
          Linking.openURL('http://maps.apple.com/?ll=37.484847,-122.148386')
        }}
      ><Text>Open Maps</Text></TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          Linking.openURL('sms://')
        }}
      ><Text>SMS Driver</Text></TouchableOpacity>
    </View>
  );
};
export default App;
