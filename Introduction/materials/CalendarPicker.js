import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';

const CalendarPickerComponent = () => {
  const [selectedStartDate, setSelectedStartDate] = useState(null)

  return (
    <View style={styles.container}>
      <CalendarPicker onDateChange={setSelectedStartDate} />
      <View>
        <Text>SELECTED DATE:{ new Date(selectedStartDate || '2020-10-10').toISOString() }</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginTop: 100,
  },
});
export default CalendarPickerComponent