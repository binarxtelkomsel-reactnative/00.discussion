import React, { useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';

const CalendarStripComponent = () => {
  const [dateSelected, setDateSelected] = useState(null)
  return (
    <View style={{width: '100%', height: '20%'}}>
      <Text>{dateSelected}</Text>
      <CalendarStrip
        scrollable
        style={{height:200, paddingTop: 20, paddingBottom: 10}}
        // calendarColor={'#F0F0F0'}
        calendarHeaderStyle={{color: 'black'}}
        dateNumberStyle={{color: 'black'}}
        dateNameStyle={{color: 'black'}}
        iconContainer={{flex: 0.1}}
        onDateSelected={date => setDateSelected(date.toString())}
      />
    </View>
  );
} 

const styles = StyleSheet.create({
  container: { flex: 1 }
});

export default CalendarStripComponent;