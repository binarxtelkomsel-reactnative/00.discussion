import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Modal, StyleSheet } from 'react-native'

const PhoneNumber = () => {
  let [data, setData] = useState({
    username: '',
    password: '',
    email: '',
    isLoading: false,
    onError: false,
    onMessage: false,
    name: '',
    avatar: null,
    id: '',
    phonenumber: [],
    KK: null,
    NIK: null,
    createdAt: null,
    updatedAt: null,
    token: null
  })
  const [newNumber, setNewNumber] = useState('')
  const [onEditNumber, setOnEditNumber] = useState({index: null, phone: ''})
  const [modalVisible, setModalVisible] = useState(false);

  const updateNumber = () => {
    let updatePhoneNumber = []
    data.phonenumber.forEach((item, i) => {
      if(i === onEditNumber.index) updatePhoneNumber.push({phone: onEditNumber.phone})
      else updatePhoneNumber.push(item)
    })
    setData({...data, phonenumber: updatePhoneNumber})
    setModalVisible(false)
    setOnEditNumber({index: null, phone: ''})
    setNewNumber('')
  }
  const deleteNumber = () => {
    let updatePhoneNumber = []
    data.phonenumber.forEach((item, i) => {
      if(i !== onEditNumber.index) updatePhoneNumber.push(item)
    })
    setData({...data, phonenumber: updatePhoneNumber})
    setModalVisible(false)
  }

  return (
    <View>
      {/* MODAL */}
       <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{color: '#555'}} >Ganti Nomor / Delete Nomor</Text>
            <TextInput 
              style={styles.modalText}
              value={onEditNumber.phone}
              keyboardType='number-pad'
              placeholder='masukkan nomor telepon'
              onChangeText={text => setOnEditNumber({...onEditNumber, phone: text})}
              onSubmitEditing={updateNumber}
            />
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={updateNumber}
            >
              <Text style={styles.textStyle}>Update</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.buttonDelete]}
              onPress={deleteNumber}
            >
              <Text style={styles.textStyle}>Delete</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* INPUT NEW NUMBER */}
      <TextInput 
        value={newNumber}
        keyboardType='number-pad'
        placeholder='masukkan nomor telepon'
        onChangeText={setNewNumber}
        onSubmitEditing={(e) => {
          if(data && data.phonenumber.length < 3) {
            setData({
              ...data, 
              phonenumber: [
                ...data.phonenumber, 
                {phone: e.nativeEvent.text}
              ]
            })
          }
          setNewNumber('')
        }} 
      />
      {/* PHONE NUMBERS */}
      <Text>Phone Numbers :</Text>
      {data.phonenumber && data.phonenumber.map((item, i) => (
        <TouchableOpacity
          key={i}
          onPress={() => {
            setOnEditNumber({index:i, phone:item.phone})
            setModalVisible(true)
          }}
        >
          <Text>phone {i+1} : {item.phone}</Text>
        </TouchableOpacity>
      ))}
    </View>
  )

}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  buttonDelete: {
    backgroundColor: "#ff0000"
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
export default PhoneNumber;