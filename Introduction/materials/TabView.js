import axios from 'axios';
import * as React from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { useState } from 'react/cjs/react.development';
 
const FirstRoute = ({data}) => {
  const [allData, setAllData] = useState([])

  React.useEffect(() => {
    setAllData(data)
  }, [])
  return (
    <View style={[styles.scene, { backgroundColor: '#ff4081' }]} key={data.id}>
      <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
        {data && data.map(el => (
          <Image
            key={el.id}
            style={{width: 100, height: 100}}
            source={{uri: el.download_url}}
          />
        ))}
      </View>
    </View>
  );
}
 
const SecondRoute = ({data}) => {
  const [allData, setAllData] = useState([])

  React.useEffect(() => {
    setAllData(data)
  }, [])

  return (
    <View style={[styles.scene, { backgroundColor: '#673ab7' }]} key={data.id}>
    <View style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
    {allData && allData.map(el => (
      <Image
        key={el.id}
        style={{width: 100, height: 100}}
        source={{uri: el.download_url}}
      />
    ))}
    </View>
  </View>
  );
} 
 
const initialLayout = { width: Dimensions.get('window').width };
 
function TabViewComponent() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'First' },
    { key: 'second', title: 'Second' },
  ]);
  const [fetch, setFetch] = useState([])
  const [fetch2, setFetch2] = useState([])

  React.useEffect(() => {
    axios({
      method: 'GET',
      url: 'https://picsum.photos/v2/list'
    })
      .then(({data}) => {
        setFetch(data)
        axios({
          method: 'GET',
          url: 'https://picsum.photos/v2/list?page=2&limit=100'
        })
          .then(({data}) => {
            setFetch2(data)
          })
          .catch(err => {
            console.log(err.message)
          })
      })
      .catch(err => {
        console.log(err.message)
      })
  }, [])
 
  const renderScene = SceneMap({
    first: () => <FirstRoute data={fetch} />,
    second: () => <SecondRoute data={fetch2} />,
  });
 
  return (
    <View style={{flex: 1}}>
      <View style={{height: 100}}></View>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
      />
    </View>
  );
}
 
const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});

export default TabViewComponent