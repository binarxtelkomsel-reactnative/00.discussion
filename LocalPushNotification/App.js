import React from 'react';
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import {
  handleCancel,
  showNotification,
  handleScheduleNotification
} from './src/notification.ios'

const App: () => React$Node = () => {
  return (
    <View style={{flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity activeOpacity={0.6} onPress={() => showNotification('Immediately', 'This one is Immediately notification')}>
        <View style={{ width: 300, height: 25, borderRadius: 50, backgroundColor: 'green' }}>
          <Text>
            Click me to show notification
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.6} onPress={() => handleScheduleNotification('Scheduled', 'This one is Scheduled notification')}>
        <View style={{ width: 300, height: 25, borderRadius: 50, backgroundColor: 'orange' }}>
          <Text>
            Click me to get notification after 5sec.
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.6} onPress={handleCancel}>
        <View style={{ width: 300, height: 25, borderRadius: 50, backgroundColor: 'honeydew' }}>
          <Text>
            Click me to cancel all notifications
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default App;
