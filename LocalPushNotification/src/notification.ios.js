import PushNotificationIOS from '@react-native-community/push-notification-ios';
// import { Alert, Linking } from 'react-native'

const showNotification = async (title, message) => {
  // OPEN LINK and OTHER APP
  // Linking.openURL('https://youtube.com')
  // Linking.openURL('exp://expo.io')

  // BASIC NOTIFICATION
  PushNotificationIOS.addNotificationRequest({
    id: 'notificationWithSound',
    title: 'Sample Title',
    subtitle: 'Sample Subtitle',
    body: 'Sample local notification with custom sound',
    // badge: 1,
  })

  // HANDLE ABILITY TO REPLY
  // PushNotificationIOS.setNotificationCategories([
  //   {
  //     id: 'test',
  //     actions: [
  //       {id: 'open', title: 'Open', options: {foreground: true}},
  //       {
  //         id: 'ignore',
  //         title: 'Desruptive',
  //         options: {foreground: true, destructive: true},
  //       },
  //       {
  //         id: 'text',
  //         title: 'Text Input',
  //         options: {foreground: true},
  //         textInput: {buttonTitle: 'Send'},
  //       },
  //     ],
  //   },
  // ]);
  // Alert.alert(
  //   'setNotificationCategories',
  //   `Set notification category complete`,
  //   [
  //     {
  //       text: 'Dismiss',
  //       onPress: null,
  //     },
  //     {
  //       text: 'Open',
  //       onPress: null
  //     }
  //   ],
  // );
}

const handleScheduleNotification = (title, message) => {
  PushNotificationIOS.addNotificationRequest({
    id: 'test',
    title: 'title',
    subtitle: 'subtitle',
    body: 'body',
    category: 'test',
    threadId: 'thread-id',
    fireDate: new Date(new Date().valueOf() + 5000),
    repeats: true,
  })
}

const handleCancel = () => {
  PushNotificationIOS.removeAllDeliveredNotifications()
}

export { showNotification, handleScheduleNotification,handleCancel }