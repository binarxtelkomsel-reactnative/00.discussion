import React, { useState } from "react";
import { TextInput, View, Text, TouchableOpacity, Linking, FlatList, ScrollView } from "react-native";

const App = () => {
  const data = [
    {id: 1, name: 'A'},
    {id: 2, name: 'B'},
    {id: 3, name: 'C'},
    {id: 4, name: 'D'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
    {id: 5, name: 'E'},
  ]
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: '50%'}}>
      {/* <TouchableOpacity
        style={{backgroundColor: 'green', width: '30%', height: '20%'}}
        onPress={openExpo}
      >
        <Text>Click Me</Text>
      </TouchableOpacity> */}
      <ScrollView>
        <FlatList
          data={data}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <Text>{item.name}</Text>
          )}
          onEndReachedThreshold={10}
          onEndReached={({ distanceFromEnd }) => {
            console.log('on end reached ', distanceFromEnd)
          }}
        />
      </ScrollView>
    </View>
  );
};

export default App