const express = require('express')
const sokcetio = require('socket.io')
const multer = require('multer')
const http = require('http')

const app = express()
const server = http.createServer(app)
const io = sokcetio(server)
const PORT = process.env.PORT || 5000
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'public/')
  },
  filename(req, file, cb) {
    cb(null, new Date().toISOString()+'-'+file.originalname)
  }
})
const upload = multer({dest: 'public/', storage})

app.use(express.static('public'))
app.use(express.urlencoded({extended:true}))

app.get('/', (req, res) => {
  res.send('you r connected')
})

app.post('/upload', upload.single('file'), (req, res) => {
  res.send({message: 'file uploaded'})
})

let messages = []

io.on('connection', (socket) => {
  socket.on('join', data => {
    console.log(data)
  })
  socket.on('sendMessage', message => {
    messages.push(message)
    socket.emit('sendMessages', messages)
  })
})

server.listen(PORT, () => console.log('running on port', PORT))