const axios = require('axios')
const FormData = require('form-data')
const form = new FormData()
const fs = require('fs')

form.append('file', fs.createReadStream('./public/2021-02-19T00:51:06.149Z-Watching.mov'))
axios.post('http://localhost:5000/upload', form, { headers: form.getHeaders() })
  .then(res => console.log(res.data))
  .catch(err => console.log(err))