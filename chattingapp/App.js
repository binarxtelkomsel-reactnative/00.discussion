import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Linking,
  Dimensions,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import socketIOClient from "socket.io-client";
const ENDPOINT = "http://127.0.0.1:5000";

const App: () => React$Node = () => {
  const [messages, setMessages] = useState([])
  const [message, setMessage] = useState('')
  const socket = socketIOClient(ENDPOINT);

  useEffect(() => {
    socket.emit('join', { name: 'wawan' })
  }, [])

  useEffect(() => {}, [messages])
  
  const sendMessage = () => {
    socket.emit('sendMessage', message)
    socket.on('sendMessages', messagesServer => {
      setMessages(messagesServer)
    })
    setMessage('')
  }

  return (
    <SafeAreaView>
      <View style={{ width: '100%', height: windowHeight }}>
        <View
          style={{width: '100%', height: windowHeight - 100, paddingRight: 10, paddingLeft: 10}}
        >
          <View style={{width: 200, height: 40, backgroundColor: '#f0f0f0', borderRadius: 50, borderTopLeftRadius: 0}}></View>
          <View style={{width: 200, height: 40, backgroundColor: '#78c5ef', borderRadius: 50, borderBottomRightRadius: 0}}></View>
        {messages && messages.map((el, i) => (
          <Text key={i}>{el}</Text>
        ))}
        </View>
        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <TextInput
            value={message}
            placeholder='input message...'
            onChangeText={setMessage}
            onSubmitEditing={() => sendMessage()}
            style={{
              width: windowWidth - 60, 
              height: 30,
            }}
            
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
export default App;
